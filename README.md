This straight up does not compile right now; a very early initial attempt at making some sort of smallish graphics library using OpenGL for a calculator app that was abandoned mid-development of an ECS a while ago due to lack of time. 

Uses:
- `gl` crate for a wrapper around OpenGL api
- `sdl2` for window/events/application loop
- `freetype-rs` for a wrapper around freetype 2 lib
- `nalgebra-glm` wrapper around C++ GLM for vector & projection math
