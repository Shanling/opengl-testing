#version 330 core
in vec2 TextureCoords;
out vec4 color;

//mono-colored bitmap image color
uniform sampler2D text;
//final font color so we can adjust it dynamically
uniform vec3 textColor;

void main() {
    //stored the color info in the font glyph texture's r only
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TextureCoords).r);
    //r g b a * sampled; sampled's 'a' value should be 0 for areas that have no color data
    //from the bitmap
    color = vec4(textColor, 1.0) * sampled;
}
