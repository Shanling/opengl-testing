// use input::InputHandler;

// use crate::input::Input;
// use crate::ui_components::app_consts::app_consts::APP_BACKGROUND_COLOR;
// use crate::ui_components::textbox::{create_body_text, TextBox};
// use crate::ui_components::util::{Padding, VideoTrait};
extern crate gl;
extern crate sdl2;
extern crate nalgebra_glm as glm;

#[macro_use]
extern crate anyhow;

use std::path::Path;

use sdl2::event::{Event, WindowEvent};
use sdl2::keyboard::Keycode;
use sdl2::video::*;

use crate::render_gl::color_buffer::ColorBuffer;
use crate::render_gl::text::{FontModule, Text, c_string_from_str};
use crate::render_gl::viewport::Viewport;
use crate::resources::Resources;
use glm::{vec4, ortho, vec2, Vec2};
use gl::types::*;
use crate::render_gl::shader::ui_component::init_entities;
use crate::modules::entity_module::{AppModule, text_box};
use crate::util::position::BOTTOM_LEFT;
use crate::util::util::dimen;
use crate::modules::app_module::AppModule;
use crate::ui_components::text_box::text_box;
use std::ops::Mul;

mod resources;
mod render_gl;
mod util;
mod modules;
mod ui_components;

// x = 2
// #comment
// y = 3 + x
// y = 5

fn main() {
    if let Err(e) = run() {
        println!("{}", e);
    }
}

fn run() -> Result<(), anyhow::Error> {
    const WIDTH: u32 = 1024;
    const HEIGHT: u32 = 768;

    let res = Resources::from_relative_exe_path(Path::new("assets"))?;
    println!("Resource root path: {}", res.root_path.as_path().to_str().unwrap());

    //contexts
    let sdl_context = sdl2::init().map_err(|e| anyhow!("Failed to initialize SDL context: {}", e))?;
    let video_subsystem = sdl_context.video().map_err(|e| anyhow!("Failed to create video subsystem: {}", e))?;
    let gl_attr = video_subsystem.gl_attr();
    gl_attr.set_context_profile(GLProfile::Core);
    gl_attr.set_context_version(4, 1);

    //application container
    let window = video_subsystem.window("Napkin", WIDTH, HEIGHT)
        .position_centered()
        .resizable()
        .opengl()
        .build()
        .map_err(|e|anyhow!("Failed to create window: {}", e))?;

    let _window_context = window.gl_create_context().map_err(|e| anyhow!("Failed to create GL context: {}", e))?;
    //this loads openGL by fetching the pointer to the OpenGL function
    gl::load_with(|name| video_subsystem.gl_get_proc_address(name) as *const _);

    debug_assert_eq!(gl_attr.context_profile(), GLProfile::Core);
    debug_assert_eq!(gl_attr.context_version(), (4, 1));

    video_subsystem.gl_set_swap_interval(SwapInterval::VSync)
        .map_err(|e| anyhow!("Failed to set swap interval: {}", e))?;

    let mut event_pump = sdl_context.event_pump()
        .map_err(|e| anyhow!("Failed to create event pump: {}", e))?;

    let mut gl_viewport = Viewport::for_window(WIDTH as i32, HEIGHT as i32);
    gl_viewport.use_viewport();

    let color_buffer = ColorBuffer::from_color(vec4(0.2, 0.2, 0.4, 1.0));
    color_buffer.use_color();

    //empty app state
    let mut app = AppModule::new();
    load_initial_ui(&mut app, &gl_viewport);

    let test_text = Text::new(&res, &gl_viewport)?
        .position(50.0, 50.0);

    'application_loop: loop {
        color_buffer.clear();

        test_text.render(&font_module, "I AM TEXT AND I am RENDERED!!")?;

        //event handling
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'application_loop;
                }
                Event::Window {
                    win_event: WindowEvent::Resized(w, h), ..
                } => {
                    gl_viewport.update_size(w, h);
                    gl_viewport.use_viewport();
                    test_text.update_projection(&gl_viewport);
                }
                _ => {}
            }
        }

        //this will move the back-buffer into the window to be presented on screen
        window.gl_swap_window();
    }

    Ok(())
}

fn load_initial_ui(app: &mut AppModule, viewport: &Viewport) {
    //test: create a text box in the bottom left corner that stretches to bottom right
    text_box(app)
        .text("Test text")
        .dimen(viewport.w, viewport.h * 0.3);
}






// let input = input_handler.process_inputs(video_sys.text_input().is_active());
// if input != Input::Unhandled {
//     println!("Input is: {:?}", input)
// }
// match input {
//     Input::UserTextInput { input } => {
//         app_state.user_input.push_str(&input);
//         println!("{}", app_state.user_input)
//     }
//     Input::UserDeleteChar => {
//         app_state.user_input.pop();
//         println!("{}", app_state.user_input)
//     },
//     Input::UserDoneEditing => {
//         video_sys.text_input().stop();
//         println!("{}", "user is done editing.")
//     }
//     Input::Quit => break 'application_loop,
//     Input::ScrollUp => {} //TODO, translate screen by some +y amount (idk how)
//     Input::ScrollDown => {} //TODO, translate screen by some +y amount (idk how)
//     Input::ExecuteCurrentLine => {} //TODO
//     Input::Click { .. } => {
//         //TODO: need to start / stop text input when user clicks in input area... to be defined bro..
//         video_sys.text_input().stop();
//     } //TODO, find intersection with line (line needs coords)
//     Input::Copy => {
//         if app_state.currently_selected_text.is_empty() {
//             println!("No text selected.")
//         } else {
//             video_sys.set_clipboard_text(&app_state.currently_selected_text)
//         }
//     }
//     Input::Paste => {
//         match video_sys.get_clipboard_text() {
//             Ok(text) => println!("Clipboard text is: {}", text),
//             Err(..) => println!("No text found in clipboard.")
//         }
//     }
//     _ => {} //implement fancy features later
// }