use crate::ui_components::cursor::Cursor;
use crate::ui_components::text_box::TextBox;
use crate::modules::entity_module::EntityModule;
use crate::modules::text_module::TextModule;
use crate::resources::Resources;

//holds all UI components
pub struct AppModule {
    pub(crate) entity_module: EntityModule,
    pub(crate) text_module: TextModule,
    pub(crate) cursor: Option<Cursor>,
}

impl AppModule {
    pub fn new(res: &Resources) -> Result<AppModule, anyhow::Error> {
        let text_module = TextModule::new(res)?;
        Ok(AppModule {
            entity_module: EntityModule::new(),
            text_module,
            cursor: None,
        })
    }

    pub fn render_entities(self) {
        //render text boxes
        self.text_module.shader.use_shader();
        for text_box in self.text_boxes {
            //bind textbox's vertex array
            //draw triangles for this text box
        }
    }
}