use std::collections::HashMap;
use std::ops::Index;
use crate::util::util::{Bounds, Dimen, dimen};
use nalgebra_glm::*;

//goals:
//Store components of same type in Vec to reduce cache misses
//components must be able to refer to other components via id

type EntityID = usize;

//entities are option because they may have been removed since last lookup
pub struct EntityModule {
    entities: Vec<Entity>
}

impl EntityModule {
    pub fn new() -> EntityModule {
        EntityModule { entities: Vec::new() }
    }

    ///insert an entity into the entities vec; Entity will have an ID with value == entities.len();
    ///
    /// format is : `Vec[Entity((1.0, 1.0), 0), Entity(2.0, 2.0, 1), ...]`
    /// ```
    /// let e = EntityModule::new()
    /// e.insert_entity()
    /// assert_eq!(e.entities.len(), 1)
    /// assert_eq!(e.entities.last(), Some(0))
    /// ```
    pub fn gen_entity(&mut self) -> EntityID {
        //entities is [0, 1, 2]
        //new_entity_value =
        let new_entity_value = self.entities.len();
        println!("Inserting entity with value: {}", new_entity_value);
        self.entities.push(entity_index);
        return entity_index;
    }

    ///```
    /// let e = EntityModule::new();
    /// e.insert_entity(); //id = 0
    /// e.insert_entity(); //id = 1
    /// e.insert_entity(); //id = 2
    /// assert_eq!(e.get_entity(2), e.entities.get(3))
    ///```
    pub fn get_entity(&self, id: EntityID) -> Option<&Entity> {
        self.entities.get(id - 1)
    }
}

pub struct Entity {
    pub id: EntityID,
    pub bounds: Bounds,
}

impl Entity {
    pub(crate) fn new(bounds: Bounds, entity_module: &mut EntityModule) -> Entity {
        Entity {
            bounds,
            id: entity_module.gen_entity(),
        }
    }

    ///Every component is aligned by bottom left coord
    pub(crate) fn position(mut self, position: Vec2) -> Entity {
        self.bounds.bottom_left = position;
        self
    }

    pub(crate) fn dimen(mut self, wh: Dimen) -> Entity {
        self.bounds.top_right = vec2(
            self.bounds.bottom_left.x + wh.w,
            self.bounds.bottom_left.y + wh.h
        );
        self
    }
}