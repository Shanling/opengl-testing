use freetype::{Face, Library};
use std::collections::HashMap;
use crate::resources::Resources;
use freetype::face::LoadFlag;

///This will hold everything related to fonts
pub struct FontModule {
    font_face: Face,
    pub(crate) char_index: HashMap<char, FontCharacter>,
}

impl FontModule {
    pub fn enable_blending() {
        unsafe {
            gl::Enable(gl::CULL_FACE);
            gl::Enable(gl::BLEND);
            gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA)
        }
    }

    fn create_quads_for_text() -> Buffers {
        let vao = VertexArray::new();
        let vbo = ArrayBuffer::new();

        vao.bind();
        vbo.bind();
        unsafe {
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (mem::size_of::<f32>() * 24) as GLsizeiptr,
                ptr::null(),
                gl::DYNAMIC_DRAW,
            );
        }

        unsafe {
            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0, //layout = 0 in shader
                4, //number of components in vertex
                gl::FLOAT, //type
                gl::FALSE, //normalized
                (4 * mem::size_of::<f32>()) as GLint, //stride aka length of 4 components in a vertex
                ptr::null(), //ptr to data (null right now)
            );
        }
        vbo.unbind();
        vao.unbind();

        Buffers { vao, vbo }
    }
}