use crate::ui_components::text_box::TextBox;
use crate::render_gl::shader::shader_program::ShaderProgram;
use crate::resources::Resources;
use freetype::{Library, Face};
use std::collections::HashMap;
use freetype::face::LoadFlag;
use nalgebra_glm::*;
use gl::types::*;
use crate::render_gl::text::Buffers;
use crate::render_gl::buffer::{VertexArray, ArrayBuffer};
use std::{mem, ptr};

//publicly accessible struct
pub struct TextModule {
    pub(crate) shader: ShaderProgram,
    pub text_boxes: Vec<TextBox>,
    freetype: FreetypeInfo
}

//private structs
struct FreetypeInfo {
    font_face: Face,
    char_index: HashMap<char, FontCharacter>
}

struct FontCharacter {
    id: u32,
    //id handle of the glyph texture
    glyph_size: I32Vec2,
    //size of the glyph
    glyph_bearing: I32Vec2,
    //offset from baseline of the glyph to the top/left
    advance: i64, //offset to advance to next glyph aka kerning
}

impl TextModule {
    pub(crate) fn new(res: &Resources, font_size: u32) -> Result<TextModule, anyhow::Error> {
        let shader_program = ShaderProgram::from_res(res, "shaders/font")?;

        let freetype_lib = Library::init().map_err(|e| anyhow!("Failed to initialize freetype: {}", e))?;

        let face = freetype_lib.new_face(
            res.root_path.join("fonts/JetBrainsMono-Regular.ttf"),
            0, //the index of the font in the folder
        ).map_err(|e| anyhow!("Failed to create font face: {}", e))?;

        //pixel font-size we'd like to extract from this face; setting width to 0
        //lets it dynamically calculate that based on the height we set.
        face.set_pixel_sizes(0, font_size).map_err(|e| anyhow!("Failed to set pixel sizes: {}", e))?;

        let mut text_module = TextModule {
            shader: shader_program,
            text_boxes: Vec::new(),
            freetype: FreetypeInfo {
                font_face: face,
                char_index: HashMap::new()
            }
        };

        text_module.pre_load_font_textures()?;

        Ok(text_module)
    }

    fn pre_load_font_textures(&mut self) -> Result<(), anyhow::Error> {
        unsafe {
            //set the pixel storage mode (how we pack pixel data into memory)
            //this means that we are setting the alignment requirement for the start
            //of each pixel row in memory to "byte-alignment" mode
            //we need this because we are storing the color data in a single byte
            gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1)
        }

        for c in 0..128 {
            //load character with FreeType library
            self.font_face.load_char(
                c as usize,
                LoadFlag::RENDER,
            ).map_err(|e| anyhow!("Failed to load char: {} from font face: {}", char::from(c), e))?;

            let char_glyph = self.font_face.glyph();
            let char_bitmap = char_glyph.bitmap();

            let mut texture: GLuint = 0;
            unsafe {
                //generate one texture, pointer to texture
                gl::GenTextures(1, &mut texture);
                //bind the texture to the target (TEXTURE_2D);
                //any operation on TEXTURE_2D target from here on out is being done to "texture"
                gl::BindTexture(gl::TEXTURE_2D, texture);
                //configure the 2 dimensional texture image
                gl::TexImage2D(
                    gl::TEXTURE_2D, //target to operate on
                    0, //level of detail number; it's gonna be a rectangle so this must be 0
                    //each element is a single red component; GL converts it to a floating point
                    //and assembles it into a RGBA element by attaching 0 for green & blue, and 1
                    //for alpha value. Each component is clamped to range 0..1
                    gl::RED as GLint, //number of color components in the texture; since this is a solid colored
                    //texture, we can just use a single value of RGBA
                    char_bitmap.width(), //how many pixels in a bitmap row
                    char_bitmap.rows(), //how many bitmap rows total in the generated glyph
                    0, //this border value must be zero for some reason.. mysterious
                    gl::RED, //format of pixel data; single color = we use GL_RED
                    gl::UNSIGNED_BYTE, //data type of pixel data
                    char_bitmap.buffer().as_ptr() as *const GLvoid, //typeless pointer to bitmap buffer
                );
                //set texture to non-repeating, clamp to edge of available area
                //"s axis" == x; clamp the x-axis of the texture to the edge
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE as i32);
                //"t axis" == y; clamp the y-axis of the texture to the edge
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE as i32);

                //say to use linear filtering when both scaling down "min" and scaling up "mag"nify
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);

                self.char_index.insert(
                    char::from(c),
                    FontCharacter {
                        id: texture,
                        glyph_size: vec2(char_bitmap.width(), char_bitmap.rows()),
                        glyph_bearing: vec2(char_glyph.bitmap_left(), char_glyph.bitmap_top()),
                        advance: char_glyph.advance().x,
                    },
                );
            }
        }
        Ok(())
    }

    //TODO: make GL element array instead and define 4 verts vs 6
    pub fn allocate_quad() -> Buffers {
        let vao = VertexArray::new();
        let vbo = ArrayBuffer::new();

        vao.bind();
        vbo.bind();
        unsafe {
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (mem::size_of::<f32>() * 24) as GLsizeiptr,
                ptr::null(),
                gl::DYNAMIC_DRAW,
            )
        }

        unsafe {
            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(
                0, //layout == 0 in shader
                4, //number of items in vertex e.g. x y z w
                gl::FLOAT, //type
                gl::FALSE, //normalized
                (4 * mem::size_of::<f32>()) as GLint, //stride aka 4 items in 1 vertex
                ptr::null() //ptr to data (not allocated yet)
            )
        }
        vao.unbind();
        vbo.unbind();

        Buffers { vao, vbo }
    }
}