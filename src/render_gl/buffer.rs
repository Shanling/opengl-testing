#![allow(dead_code)]

use gl::types::*;

pub struct ArrayBuffer {
    pub id: GLuint
}

impl ArrayBuffer {
    pub fn new() -> ArrayBuffer {
        let mut vbo: GLuint = 0;
        unsafe {
            gl::GenBuffers(1, &mut vbo)
        }

        ArrayBuffer { id: vbo }
    }

    pub fn bind(&self) {
        println!("Binding array buffer: {}", self.id);
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.id)
        }
    }

    pub fn unbind(&self) {
        println!("Unbinding all array buffers.");
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, 0)
        }
    }
}

impl Drop for ArrayBuffer {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteBuffers(1, &mut self.id);
        }
    }
}

pub struct VertexArray {
    pub id: GLuint
}

impl VertexArray {
    pub fn new() -> VertexArray {
        let mut vao: GLuint = 0;
        unsafe {
            gl::GenVertexArrays(1, &mut vao)
        }

        VertexArray { id: vao }
    }

    pub fn bind(&self) {
        println!("Binding vertex array: {}", self.id);
        unsafe {
            gl::BindVertexArray(self.id)
        }
    }

    pub fn unbind(&self) {
        println!("Unbinding all vertex arrays.");
        unsafe {
            gl::BindVertexArray(0);
        }
    }

    pub fn enable_attrib(&self, attrib_id: u32) {
        unsafe {
            gl::EnableVertexAttribArray(attrib_id);
        }
    }
}

impl Drop for VertexArray {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &mut self.id)
        }
    }
}