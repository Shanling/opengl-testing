#![allow(dead_code)]

use glm::Vec4;

pub struct ColorBuffer {
    pub color: Vec4
}

impl ColorBuffer {
    pub fn from_color(color: Vec4) -> ColorBuffer {
        ColorBuffer {
            color: glm::vec4(color.x, color.y, color.z, color.w)
        }
    }

    pub fn update_color(&mut self, color: Vec4) {
        self.color = glm::vec4(color.x, color.y, color.z, color.w);
    }

    pub fn use_color(&self) {
        unsafe {
            gl::ClearColor(self.color.x, self.color.y, self.color.z, self.color.w)
        }
    }

    pub fn clear(&self) {
        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }
    }
}