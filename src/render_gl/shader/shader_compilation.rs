pub mod shader {
    use std::ffi::{CStr, CString};
    use std::ptr;

    use gl;
    use gl::types::*;
    use thiserror::Error;

    use crate::resources::{ResourceError, Resources};
    use crate::render_gl::shader::shader_compilation::shader::ShaderError::{UnknownShaderType, ResourceLoadingError, CompileError};
    use crate::util::util::create_blank_cstring;

    pub struct Shader {
        pub id: GLuint
    }

    #[derive(Error, Debug)]
    pub enum ShaderError {
        #[error("ShaderError -- Unknown shader type of name: ${name:?}")]
        UnknownShaderType { name: String },
        #[error("ShaderError -- Resource Loading Error: ${error:?}")]
        ResourceLoadingError { error: ResourceError },
        #[error("ShaderError -- Compile error for shader: ${shader_name:?}, reason: ${error_message:?}")]
        CompileError { shader_name: String, error_message: String },
        #[error("ShaderError -- Link error for program: ${program_name:?}, reason: ${error_message:?}")]
        LinkError { program_name: String, error_message: String },
    }

    impl Shader {
        pub fn from_res(res: &Resources, name: &str) -> Result<Shader, ShaderError> {
            //an array of str to GLenum map
            //add when creating more shader program types
            const ALL_SUPPORTED_EXTENSIONS: [(&str, GLenum); 2] = [
                (".vert", gl::VERTEX_SHADER),
                (".frag", gl::FRAGMENT_SHADER)
            ];

            let shader_kind = ALL_SUPPORTED_EXTENSIONS.iter().find(
                |&&(file_extension, _)| {
                    name.ends_with(file_extension)
                }
            ).map(|&(_, kind)| kind)
                .ok_or_else(|| UnknownShaderType { name: name.to_owned() })?;

            let source = res.load_cstring(name)
                .map_err(|e| ResourceLoadingError { error: e })?;

            Shader::from_source(&source, shader_kind)
        }

        fn from_source(
            source: &CStr,
            shader_type: GLenum,
        ) -> Result<Shader, ShaderError> {
            let shader_id = compile_shader(shader_type, source)?;
            Ok(Shader { id: shader_id })
        }
    }

    impl Drop for Shader {
        fn drop(&mut self) {
            unsafe { gl::DeleteShader(self.id) }
        }
    }

    fn compile_shader(shader_type: GLenum, shader_source: &CStr) -> Result<GLuint, ShaderError> {
        println!(
            "compiling shader type: {}",
            if shader_type == gl::VERTEX_SHADER { "vertex" } else { "fragment" }
        );
        let shader_id: GLuint = unsafe { gl::CreateShader(shader_type) };

        unsafe {
            gl::ShaderSource(shader_id, 1, &shader_source.as_ptr(), ptr::null());
            gl::CompileShader(shader_id);
        }

        let mut shader_result: GLint = 0;
        unsafe {
            gl::GetShaderiv(shader_id, gl::COMPILE_STATUS, &mut shader_result);
        }
        return if shader_result == 0 {
            //return error
            println!("compiling shader type: {} failed.",
                     if shader_type == gl::VERTEX_SHADER { "vertex" } else { "fragment" }
            );
            let mut error_log_length: i32 = 0;
            unsafe {
                gl::GetShaderiv(shader_id, gl::INFO_LOG_LENGTH, &mut error_log_length);
            }
            let error = create_blank_cstring(error_log_length as usize);

            unsafe {
                gl::GetShaderInfoLog(
                    shader_id,
                    error_log_length,
                    &mut error_log_length,
                    //this needs to be a raw char pointer to a char array which is the length of the error log
                    error.as_ptr() as *mut GLchar,
                )
            }

            Err(CompileError {
                shader_name: (if shader_type == gl::VERTEX_SHADER { "vertex" } else { "fragment" }).to_owned(),
                error_message: error.to_string_lossy().as_ref().to_owned(),
            })
        } else {
            Ok(shader_id)
        };
    }
}