use crate::resources::Resources;
use std::ptr;
use crate::render_gl::shader::shader_compilation::shader::{Shader, ShaderError};
use gl::types::*;
use crate::util::util::create_blank_cstring;
use crate::render_gl::shader::shader_compilation::shader::ShaderError::LinkError;
use crate::render_gl::shader::shader_uniform_trait_impls::Uniformable;
use anyhow::Error;

pub struct ShaderProgram {
    pub id: u32,
}

impl ShaderProgram {
    pub fn from_res(res: &Resources, name: &str) -> Result<ShaderProgram, ShaderError> {
        const ALL_SUPPORTED_EXTENSIONS: [&str; 2] = [".vert", ".frag"];

        let shaders = ALL_SUPPORTED_EXTENSIONS.iter()
            .map(|file_extension| {
                Shader::from_res(res, &format!("{}{}", name, file_extension))
            })
            .collect::<Result<Vec<Shader>, ShaderError>>()?;

        ShaderProgram::from_shaders(&shaders[..])
    }

    pub fn from_shaders(shaders: &[Shader]) -> Result<ShaderProgram, ShaderError> {
        let shader_program_id: u32 = unsafe { gl::CreateProgram() };

        for shader in shaders {
            unsafe { gl::AttachShader(shader_program_id, shader.id) }
        }

        unsafe { gl::LinkProgram(shader_program_id) }
        //error handling
        let mut shader_link_result: GLint = 0;
        unsafe {
            //same as shader error handling, store the result in the gluint ^
            gl::GetProgramiv(shader_program_id, gl::LINK_STATUS, &mut shader_link_result);
        }

        return if shader_link_result == 0 {
            //error
            let mut error_log_length: GLint = 0;
            unsafe {
                gl::GetProgramiv(shader_program_id, gl::INFO_LOG_LENGTH, &mut error_log_length);
            }
            let error = create_blank_cstring(error_log_length as usize);

            unsafe {
                gl::GetProgramInfoLog(
                    shader_program_id,
                    error_log_length,
                    ptr::null_mut(),
                    error.as_ptr() as *mut GLchar,
                )
            }
            Err(
                LinkError {
                    program_name: shader_program_id.to_string(),
                    error_message: error.to_string_lossy().as_ref().to_owned(),
                }
            )
        } else {
            for shader in shaders {
                unsafe { gl::DetachShader(shader_program_id, shader.id) }
            };

            Ok(ShaderProgram {
                id: shader_program_id,
            })
        };
    }

    pub fn use_shader(&self) {
        unsafe { gl::UseProgram(self.id) }
    }
}

impl Drop for ShaderProgram {
    fn drop(&mut self) {
        unsafe { gl::DeleteProgram(self.id) }
    }
}