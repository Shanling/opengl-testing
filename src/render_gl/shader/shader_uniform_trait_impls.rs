use anyhow::Error;
use crate::render_gl::text::c_string_from_str;
use gl::types::*;
use nalgebra_glm::{TMat4, Vec3};

pub trait Uniformable {
    fn gl_set(shader_program_id: u32, name: &str, value: Self) -> Result<(), Error>;
}

//what does a uniform need?
pub struct Uniform<T: Uniformable> {
    pub shader_program_id: u32,
    pub name: String,
    pub value: T
}

impl<T: Uniformable> Uniform<T> {
    pub fn update(&self, new_value: T) -> Result<(), Error> {
        T::gl_set(self.shader_program_id, &self.name, new_value)
    }
}

//shader program: ID, struct of uniforms
impl Uniformable for TMat4<f32> {
    fn gl_set(shader_program_id: u32, name: &str, value: Self) -> Result<(), Error> {
        unsafe {
            gl::UseProgram(shader_program_id);
            gl::UniformMatrix4fv(
                gl::GetUniformLocation(
                    shader_program_id,
                    c_string_from_str(&name)?.as_ptr()
                ),
                1,
                gl::FALSE,
                value.as_ptr() as *const GLfloat
            )
        }
        Ok(())
    }
}

impl Uniformable for Vec3 {
    fn gl_set(shader_program_id: u32, name: &str, value: Self) -> Result<(), Error> {
        unsafe {
            gl::UseProgram(shader_program_id);
            gl::Uniform3f(
                gl::GetUniformLocation(
                    shader_program_id,
                    c_string_from_str(name)?.as_ptr()
                ),
                value.x,
                value.y,
                value.z
            )
        }
        Ok(())
    }
}