#![allow(dead_code)]
extern crate freetype;

use self::freetype::{Library, Face};
use self::freetype::face::LoadFlag;
use gl::types::*;
use crate::resources::Resources;
use std::collections::HashMap;
use crate::render_gl::buffer::{ArrayBuffer, VertexArray};
use std::{mem, ptr};
use std::ffi::CString;
use crate::util::colors::{Color, RED};
use crate::render_gl::viewport::Viewport;
use glm::*;
use crate::render_gl::shader::shader_program::{ShaderProgram};
use crate::render_gl::shader::shader_uniform_trait_impls::Uniform;
use crate::render_gl::shader::ui_component::UIComponent;

//every component should have:
//shader program
pub struct TextShaderUniforms {
    pub projection_matrix: Uniform<TMat4<f32>>,
    pub text_color: Uniform<Vec3>
}

impl TextShaderUniforms {
    fn new(shader_program_id: u32, w: f32, h: f32) -> TextShaderUniforms {
        TextShaderUniforms {
            projection_matrix: Uniform {
                shader_program_id,
                name: "projection".into(),
                value: ortho(
                    0.0, //left
                    w, //right
                    0.0, //bottom
                    h, //top
                    -1.0, //near
                    1.0 //far
                )
            },
            text_color: Uniform {
                shader_program_id,
                name: "textColor".into(),
                value: RED.into()
            }
        }
    }
}

impl Text {
    pub fn update_projection(&self, viewport: &Viewport)-> Result<(), anyhow::Error> {
        self.shader_program.uniforms.unwrap().projection_matrix.update(
            ortho(
                0.0,
                viewport.w as f32,
                0.0,
                viewport.h as f32,
                -1.0,
                0.0
            )
        )
    }
}

pub struct Buffers {
    pub(crate) vao: VertexArray,
    pub(crate) vbo: ArrayBuffer,
}

pub fn c_string_from_str(original_string: &str) -> Result<CString, anyhow::Error> {
    let c_string = CString::new(original_string).map_err(|e| anyhow!("Failed to make C string: {}", e))?;
    Ok(c_string)
}