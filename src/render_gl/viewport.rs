use std::fmt::{Display, Formatter};
use std::fmt;

pub struct Viewport {
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32
}

impl Viewport {
    pub fn for_window(w: i32, h: i32) -> Viewport {
        Viewport {
            x: 0,
            y: 0,
            w,
            h
        }
    }

    pub fn update_size(&mut self, w: i32, h: i32) {
        self.w = w;
        self.h = h;
    }

    pub fn use_viewport(&self) {
        unsafe {
            gl::Viewport(self.x, self.y, self.w, self.h)
        }
    }

    pub fn get_gl_viewport(store: &mut [i32; 4]) {
        unsafe {
            gl::GetIntegerv(gl::VIEWPORT, store.as_mut_ptr());
        }
    }
    // gl::GetIntegerv(gl::VIEWPORT, viewport_w_h.as_mut_ptr());

}

impl Display for Viewport {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Viewport - x: {}, y: {}, w: {}, h: {}", self.x, self.y, self.w, self.h)
    }
}