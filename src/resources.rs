use std::path::{PathBuf, Path};
use std::env::current_exe;
use std::{io};
use std::ffi::CString;
use std::fs::File;
use std::io::Read;
use std::fmt::{Debug};
use thiserror::Error;

//derive Debug will auto-implement the Debug trait for Error enum, so we can print it
//with the :? formatter
#[derive(Error, Debug)]
pub enum ResourceError {
    #[error("ResourcesError -- IO Error: {io_error:?}")]
    Io { io_error: io::Error }, //for file & networking function errors
    #[error("ResourcesError -- File contains null char.")]
    FileContainsNullChar,
    #[error("ResourcesError -- Failed to get EXE path.")]
    FailedToGetExePath,
}

//to convert io Error to our Error enum type
impl From<io::Error> for ResourceError {
    fn from(error: io::Error) -> Self {
        ResourceError::Io {
            io_error: error
        }
    }
}

pub struct Resources {
    pub root_path: PathBuf,
}

impl Resources {
    //the executable meaning the currently running program that is calling this function
    pub fn from_relative_exe_path(relative_path: &Path) -> Result<Resources, ResourceError> {
        //?; notation at end will unwrap the Ok result into whatever type the Ok is,
        //or if it's an Err it will exit with the FailedToGetExePath error enum type that we specified
        let exe_file_name = current_exe().map_err(|_| ResourceError::FailedToGetExePath)?;
        let exe_path = exe_file_name.parent().ok_or(ResourceError::FailedToGetExePath)?;
        //join will create an owned PathBuf from the path
        Ok(Resources {
            root_path: exe_path.join(relative_path)
        })
    }

    pub fn load_cstring(&self, resource_name: &str) -> Result<CString, ResourceError> {
        let mut file = File::open(
            resource_name_to_path(&self.root_path, resource_name)
        )?;

        //allocate buffer same size as the file
        //u8 == C-char
        //usize + 1 to account for 0 byte at the end of the file
        let mut buffer: Vec<u8> = Vec::with_capacity(file.metadata()?.len() as usize + 1);
        file.read_to_end(&mut buffer)?;

        //make sure no null bytes
        //the ** will deference b (byte) twice because the iter() function returns an iterator
        //over the slice references, and then the closure in the find function receives a reference
        //over that iterator. whack
        if buffer.iter().find(|b| **b == 0).is_some() {
            return Err(ResourceError::FileContainsNullChar);
        }

        Ok(unsafe { CString::from_vec_unchecked(buffer)})
    }
}

fn resource_name_to_path(root_dir: &Path, location: &str) -> PathBuf {
    //into will cast it to a PathBuf (specified type)
    //root dir e.g. "usr/shan/home"
    let mut path: PathBuf = root_dir.into();

    //e.g. "blah/foo/bar/whee/woot.vert"
    for part in location.split("/") {
        //part = "blah", append "blah" to current "path"
        path = path.join(part);
        //path is now "usr/shan/home/blah, repeat for all split parts
    }
    //return standardized path
    path
}