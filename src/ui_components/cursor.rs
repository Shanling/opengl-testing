use crate::util::colors::{Color, WHITE};
use crate::ui_components::ui_component::UIComponent;
use crate::modules::app_module::AppModule;

pub struct Cursor {
    entity: Entity,
    color: Color,
    speed: f32,
}

impl UIComponent for Cursor {
    fn register(app_module: &mut AppModule) {
        app_module.cursor = Self
    }
}

impl Cursor {
    fn new(position: (f32, f32), app_module: &mut AppModule) -> Cursor {
        Cursor {
            entity: Entity::new(position, &mut app_module.entity_module),
            color: WHITE,
            speed: 0.0,
        }
    }

    fn move_to(&mut self, new_position: (f32, f32)) {
        self.entity.position = new_position
    }
}