use crate::util::text_util::TextOptions;
use crate::util::colors::{Color, APP_BG_COLOR};
use crate::modules::app_module::AppModule;
use crate::util::util::{Bounds, dimen, Dimen, pos};
use crate::ui_components::ui_component::UIComponent;
use crate::modules::entity_module::Entity;
use nalgebra_glm::*;
use crate::modules::text_module::TextModule;
use crate::modules::font_module::FontModule;
use std::mem;
use crate::render_gl::text::Buffers;

impl UIComponent for TextBox {
    fn register(app: &mut AppModule) {
        app.text_boxes.push(Self)
    }
}

pub struct TextBox {
    entity: Entity, //TODO: just entity ID reference? fml
    pub text: Option<String>,
    pub options: TextOptions,
    pub background_color: Color,
    pub editable: bool,
    buffers: Buffers
}

//builder
pub fn text_box(app: &mut AppModule) -> TextBox {
    TextBox {
        entity: Entity::new(
            Bounds::from(
                pos(0.0, 0.0),
                dimen(0.0, 0.0)),
            &mut app.entity_module,
        ),
        text: None,
        options: TextOptions::default(),
        background_color: APP_BG_COLOR,
        editable: false,
        buffers: TextModule::allocate_quad()
    }
}

impl TextBox {
    pub fn text(mut self, text: &str) -> TextBox {
        self.text = text.into();
        self
    }

    pub fn text_color(mut self, color: Color) -> TextBox {
        self.options.color = color;
        self
    }

    pub fn text_size(mut self, size: u8) -> TextBox {
        self.options.size = size;
        self
    }

    pub fn bg_color(mut self, color: Color) -> TextBox {
        self.background_color = color;
        self
    }

    pub fn make_editable(mut self) -> TextBox {
        self.editable = true;
        self
    }

    //this is true for every entity.. abstract out somehow?? TODO
    pub fn position(mut self, x: f32, y: f32) -> TextBox {
        self.entity.position(vec2(x, y));
        self
    }

    pub fn dimen(mut self, width: i32, height: i32) -> TextBox {
        self.entity.dimen(dimen(width as f32, height as f32));
        self
    }

    //maybe just move this into new() //TODO
    pub fn gpu_load(mut self, font_module: &FontModule) {
        unsafe {
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindVertexArray(self.buffers.vao.id);
        }

        let mut x_coord = self.position.x;

        for c in self.text.chars() {
            let font_char = font_module.char_index.get(&c);

            match font_char {
                None => {
                    println!("Could not render font char: '{}'. \
                    Reason: Not found in pre-loaded font glyphs. Skipping.", c)
                }
                Some(char) => {
                    let x_position: f32 = x_coord + (char.glyph_bearing.x as f32 * self.font_scale);
                    let y_position: f32 = self.position.y - ((char.glyph_size.y - char.glyph_bearing.y) as f32 * self.font_scale);
                    let quad_width: f32 = char.glyph_size.x as f32 * self.font_scale;
                    let quad_height: f32 = char.glyph_size.y as f32 * self.font_scale;

                    //now we need to update the VBO dynamically for each character glyph
                    //using its freetype data
                    //we already have a VBO with 6 vertices of 4 floats each that we can manipulate here
                    let new_vertices = vec![
                        //one triangle
                        vec4(x_position, y_position + quad_height, 0.0, 0.0),
                        vec4(x_position, y_position, 0.0, 1.0),
                        vec4(x_position + quad_width, y_position, 1.0, 1.0),
                        //second triangle
                        vec4(x_position, y_position + quad_height, 0.0, 0.0),
                        vec4(x_position + quad_width, y_position, 1.0, 1.0),
                        vec4(x_position + quad_width, y_position + quad_height, 1.0, 0.0)
                    ];

                    unsafe {
                        //render glyph texture OVER quad we made
                        gl::BindTexture(gl::TEXTURE_2D, char.id);
                        //update content of VBO memory
                        gl::BindBuffer(gl::ARRAY_BUFFER, self.buffers.vbo.id);
                        gl::BufferSubData(
                            gl::ARRAY_BUFFER, //target
                            0, //offset
                            (mem::size_of::<f32>() * 24) as GLsizeiptr, //size of buffer
                            new_vertices.as_ptr() as *const GLvoid, //pointer to data
                        );
                        //unbind
                        gl::BindBuffer(gl::ARRAY_BUFFER, 0);
                        //render quad
                        gl::DrawArrays(gl::TRIANGLES, 0, 6); //vertices
                        x_coord += (char.advance >> 6) as f32 * self.font_scale;
                    }
                }
            }
        }

        unsafe {
            gl::BindVertexArray(0);
            gl::BindTexture(gl::TEXTURE_2D, 0);
        }
    }

    pub fn update_on_resize()
}