use crate::modules::entity_module::{AppModule, Entity};
use crate::modules::app_module::AppModule;
use nalgebra_glm::Vec2;

//certain traits require certain modules
//e.g. a textbox requires the font_module to be loaded
pub trait UIComponent {
    fn register(app: &mut AppModule);
}