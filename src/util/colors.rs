#![allow(dead_code)]

use glm::Vec3;

pub const WHITE: Color = Color {
    r: 1.0,
    g: 1.0,
    b: 1.0,
    a: 1.0
};

pub const RED: Color = Color {
    r: 1.0,
    g: 0.0,
    b: 0.0,
    a: 1.0
};

pub const APP_BG_COLOR: Color = Color {
    r: 0.15,
    g: 0.20,
    b: 0.22,
    a: 1.0
};

pub struct Color {
    r: f32,
    g: f32,
    b: f32,
    a: f32
}

impl Into<Vec3> for Color {
    fn into(self) -> Vec3 {
        glm::vec3(self.r, self.g, self.b)
    }
}
