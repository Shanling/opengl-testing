use nalgebra_glm::*;

//screen coords:
// (0, h)         (w,h)
//   |-------------|
//   |             |
//   |             |
//   |             |
//   |-------------|
// (0,0)          (w,0)


pub const BOTTOM_LEFT: Vec2 = vec2(0.0, 0.0);

pub fn bottom_right(viewport_width: i32) -> Vec2 {
    vec2(viewport_width as f32, 0.0)
}

pub fn top_right(viewport_width: i32, viewport_height: i32) -> Vec2 {
    vec2(viewport_width as f32, viewport_height as f32)
}

pub fn top_left(viewport_height: i32) -> Vec2 {
    vec2(0.0, viewport_height as f32)
}

///for positioning ui components
enum Position {
    Relative {
        to: EntityId,
        offset: Vec4 //from center? i guess?
    },
    Absolute { //TODO: clamp absolute position to gl viewport bounds?
    x: i32,
        y: i32
    },
    Centered,
    CenterTop,
    CenterRight,
    CenterLeft,
    CenterBottom
}