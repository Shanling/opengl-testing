//=======

use crate::util::colors::{Color, WHITE};

pub struct TextOptions {
    pub color: Color,
    pub size: u8,
}

impl TextOptions {
    pub fn new(color: Color, size: u8) -> TextOptions {
        TextOptions { color, size }
    }

    pub fn default() -> TextOptions {
        TextOptions { color: WHITE, size: 16 }
    }
}