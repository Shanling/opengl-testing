use std::ffi::CString;
use nalgebra_glm::*;

//(x, y) -> bottom left, (x, y) -> top right coord of bounds.
//since we are only working in 2D, bounds just define a box (for now)
pub struct Bounds {
    pub(crate) bottom_left: Vec2,
    pub(crate) top_right: Vec2
}

impl Bounds {
    pub fn from(position: Vec2, dimen: Dimen) -> Bounds {
        Bounds {
            bottom_left: position,
            //TODO: this aint gonna compile.. or is it?
            top_right: vec2(&position.x + dimen.w, &position.y + dimen.h)
        }
    }
}

impl Into<Vec4> for Bounds {
    fn into(self) -> Vec4 {
        vec4(self.bottom_left.x, self.bottom_left.y, self.top_right.x, self.top_right.y)
    }
}

pub struct Dimen {
    pub w: f32,
    pub h: f32
}

impl Dimen {
    pub fn new(width: f32, height: f32) -> Dimen {
        Dimen(vec2(width, height))
    }
}

pub fn dimen(width: f32, height: f32) -> Dimen {
    Dimen::new(width, height)
}

pub fn create_blank_cstring(len: usize) -> CString {
    let buffer = vec![b' '; len as usize + 1];
    unsafe { CString::from_vec_unchecked(buffer) }
}

pub fn pos(x: f32, y: f32) -> Vec2 {
    vec2(x, y)
}